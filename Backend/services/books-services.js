const getAllBooks = (booksData) => {
    return booksData.getAllBooks();
   };


   const getAllGenres = (booksData) => {
    return booksData.getAllGenres();
   };
  
   const getBookById = (booksData) => {
    return async (id) => {
      const book = await booksData.getBy("id", id);
  
     
  
      return { error: null, book: book };
    };
   };


   const getBookByGenre = (booksData) => {
    return async (genre) => {
      const book = await booksData.getBy("genre", genre);
  
     
  
      return { error: null, book: book };
    };
   };



   export default {
    getAllBooks,
    getAllGenres,
    getBookById,
    getBookByGenre,
    
   };