import pool from "./pool.js"

const getAllBooks = async () => {
    const sql =
    `SELECT id, name
    FROM books`;
  
    return await pool.query(sql);
  };
  
  const getBy = async (column, value) => {
    const sql =
    `SELECT id, name
    FROM books
    WHERE ${column} = ?`;
  
    const result = await pool.query(sql, [value]);
  
    return result[0];
  };

  const getAllGenres = async () => {
    const sql =
    `SELECT genreId, name
    FROM genres`;
  
    return await pool.query(sql);
  };

  export default {
    getAllBooks,
    getBy,
    getAllGenres
  };