import express from "express";
import booksData from '../data/books-data.js';
import booksService from "../services/books-services.js";




const booksController = express.Router();


booksController

  // get all books
  .get("/", async (req, res) => {
    const books = await booksService.getAllBooks(booksData);
   
    res.status(200).send(books);
  })

  //get all genres

  .get("/genres", async (req, res) => {
    const genres = await booksService.getAllGenres(booksData);
    res.status(200).send(genres);
  })


  // get book by id
  .get("/:id", async (req, res) => {
    const id = +req.params.id;


    const { error, book } = await booksService.getBookById(booksData)(id);
   
   
        res.status(200).send(book);
      
    }
  )


  //get bok by genre (gives you one book from the particular genre)

  .get("/genres/:name", async (req, res) => {
    const genre = req.params.name;

console.log(req.params.name);
    const { error, book } = await booksService.getBookByGenre(booksData)(genre);
   
   
        res.status(200).send(book);
      
    }
  )

  export default booksController;