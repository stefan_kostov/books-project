import { useContext, useEffect, useState } from "react";
import {AuthContext} from "../Providers/AuthContext"
import { useNavigate } from "react-router";



const Header = () => {
    const { user, setUser } = useContext(AuthContext);
    const history = useNavigate();
    let [checked, setChecked] = useState(false);
  
    const goBack = () => history.push("/home");
  
    const refreshPage = () => {
      window.location.reload();
    };
  
    const triggerLogout = () => {
      setUser({ isLoggedIn: false, user: null });
      localStorage.removeItem("token");
      goBack();
    };
  
    const [userInfo, setUserInfo] = useState(null);
  
    useEffect(() => {
      fetch(`http://localhost:5000/users/${user?.sub}`, {
        headers: { Authorization: `Bearer ${localStorage.getItem("token")}` },
      })
        .then((result) => result.json())
        .then((data) => setUserInfo(data));
    }, [user?.sub]);
  
    return (
      <div className="Header page-wrapper">

          </div>
           );
        };
        
        export default Header;