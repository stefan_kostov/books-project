import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useState } from 'react';
import Header from "./Authentication/Header/Header";
import {getUserFromToken} from "./Authentication/Utils/token";
import { AuthContext } from './Authentication/Providers/AuthContext';



import './App.css';

function App() {

  const [authValue, setAuthValue] = useState ({
    isLoggedIn: !!getUserFromToken(localStorage.getItem('token')),
    user: getUserFromToken(localStorage.getItem('token')),
  });


  return (
    <div className="App">
      <header className="App-header">
        <BrowserRouter>
        <AuthContext.Provider value={{...authValue, setUser: setAuthValue}}>
        
        <Routes>
        
        <Route exact path="/home" component= {Header} />
        <Route exact path="/auth/login" component= {Header} />
        
        <Route exact path="/courses/:id" auth={authValue.isLoggedIn} component= {Header} />
        
        </Routes>
        
        </AuthContext.Provider>
        </BrowserRouter>
      </header>
    </div>
  );
}

export default App;
